
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>CoronaStatus</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        
    </head>
    <body>
        <main class="w-100 p-3 bg-linear text-dark text-center d-flex flex-column align-items-center justify-content-center" style="min-height: 100vh;">
            <div class="w-100 p-3" style="max-width: 650px">
                <div class="card rounded-0 shadow w-100 p-md-3 p-2">
                    <div class="card-body">
                        <h3 class="card-subtitle color">Corona Status</h3>
                        <form class="mt-4" action="{{ route('register') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <input 
                                    type="text" id="name" name="name" value="{{ old('name') }}" placeholder="Nome completo"
                                    class="form-control rounded-0 text-center @error('name') is-invalid @enderror" required
                                />
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input 
                                    type="email" id="email" name="email" value="{{ old('email') }}" placeholder="Email"
                                    class="form-control rounded-0 text-center @error('email') is-invalid @enderror" required
                                />
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input 
                                    type="password" id="password" name="password" value="{{ old('password') }}" placeholder="Senha"
                                    class="form-control rounded-0 text-center @error('password') is-invalid @enderror" required
                                />
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input 
                                    type="password" id="password-confirm" name="password_confirmation" value="{{ old('password') }}" placeholder="Senha"
                                    class="form-control rounded-0 text-center @error('password') is-invalid @enderror" required
                                />
                            </div>
                            <button class="btn btn-outline btn-custom btn-block rounded-0 my-3">Cadastrar-se</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>
