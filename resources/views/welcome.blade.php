<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Corona Status</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}"/>

        <style>
            .jump {
                animation: jump 600ms linear infinite;
                animation-direction: alternate
            }

            .divider {
                border: 1px solid #9408bf;
                width: 12%
            }

            @keyframes jump {
                from {
                    transform: translateY(0px)
                }
                to {
                    transform: translateY(10px)
                }
            }
        </style>
    </head>
    <body>
        <main class="bg-linear w-100 p-3 text-light text-center d-flex flex-column align-items-center justify-content-between" style="min-height: 100vh;">
            <nav class="navbar navbar-dark justify-content-center justify-content-sm-between w-100">
                <h1 class="navbar-brand font-weight-bold d-none d-sm-block">Corona Status</h1>
                <ul class="navbar-nav d-flex flex-row">
                    <li class="nav-item mx-2">
                        <a class="nav-link" href="{{ route('register') }}">Cadastre-se</a>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link" href="{{ route('login') }}">Entrar</a>
                    </li>
                </ul>
            </nav>
            
            <div class="d-flex flex-column align-items-center my-3">
                <!-- TITLES -->
                <h1 class="display-4 font-weight-normal d-none d-lg-block">Corona Status</h1>
                <h1 class="font-weight-light d-block d-lg-none">Corona Status</h1>

                <h3 class="font-weight-light mt-3">Colabore e saiba como está o risco de contaminação de coronavirus a sua volta.</h3>
            </div>

            <div class="d-flex flex-column align-items-center mb-2">
                <h5>Vamos começar?</h5>
                <svg class="bi bi-chevron-down jump" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 01.708 0L8 10.293l5.646-5.647a.5.5 0 01.708.708l-6 6a.5.5 0 01-.708 0l-6-6a.5.5 0 010-.708z" clip-rule="evenodd"/>
                </svg>
            </div>
        </main>

        <section class="w-100 pt-4 container text-dark text-center d-flex flex-column align-items-center">
            <h1 class="mb-3">Porque usar</h1>
            <div class="d-flex flex-wrap w-100">
                <article class="card col-md-4 border-0 bg-transparent">
                    <div class="card-body d-flex flex-column align-items-center">
                        <h2 class="color">Escalável</h2>
                        <div class="dropdown-divider divider" style="max-width: 100px"></div>
                        <h4 class="font-weight-normal text-dark">Quanto mais pessoas colaboram, mais eficiente se torna a plataforma.</h4>
                    </div>
                </article>
                <article class="card col-md-4 border-0 bg-transparent">
                    <div class="card-body d-flex flex-column align-items-center">
                        <h2 class="color">Conscientização</h2>
                        <div class="dropdown-divider divider" style="max-width: 100px"></div>
                        <h4 class="font-weight-normal text-dark">Ajudamos as pessoas a se conscientizarem sobre a situação que estamos passando.</h4>
                    </div>
                </article>
                <article class="card col-md-4 border-0 bg-transparent">
                    <div class="card-body d-flex flex-column align-items-center">
                        <h2 class="color">Simples e Prático</h2>
                        <div class="dropdown-divider divider" style="max-width: 100px"></div>
                        <h4 class="font-weight-normal text-dark">O processo de criação de conta e postagem é extremamente rápido.</h4>
                    </div>
                </article>
            </div>
        </section>

        <section class="w-100 pt-4 container text-dark text-center d-flex flex-column align-items-center">
            <h1 class="mb-3">Como funciona</h1>
            <div class="d-flex flex-wrap w-100">
                <article class="card col-md-4 border-0 bg-transparent">
                    <div class="card-body d-flex flex-column align-items-center">
                        <h2 class="color">Criando sua conta</h2>
                        <div class="dropdown-divider divider" style="max-width: 100px"></div>
                        <h4 class="text-dark font-weight-normal">
                            Você pode 
                            <a href="#" class="color text-decoration-none"> criar sua conta </a>
                            rapidamente fornecendo apenas algumas informações básicas que irão colaborar com o funcionamento da plataforma.
                        </h4>
                    </div>
                </article>
                <article class="card col-md-4 border-0 bg-transparent">
                    <div class="card-body d-flex flex-column align-items-center">
                        <h2 class="color">Postando</h2>
                        <div class="dropdown-divider divider" style="max-width: 100px"></div>
                        <h4 class="text-dark font-weight-normal">
                            Após já ter uma conta criada, você faz posts dizendo para as pessoas ao seu redor como você está se sentindo, assim elas saberão
                            se existem casos suspeitos e risco de contaminação próximo a elas.
                        </h4>
                    </div>
                </article>
                <article class="card col-md-4 border-0 bg-transparent">
                    <div class="card-body d-flex flex-column align-items-center">
                        <h2 class="color">O feed</h2>
                        <div class="dropdown-divider divider" style="max-width: 100px"></div>
                        <h4 class="text-dark font-weight-normal">
                            Assim como as pessoas irão ver posts de pessoas próximas a elas, você também vai, então a etapa acima serve para você no sentindo inverso também.
                        </h4>
                    </div>
                </article>
            </div>
        </section>

        <footer class="w-100 justify-content-center text-center text-dark my-3">
            <span class="h5">Desenvolvido por <a href="#" rel="noopener" class="color font-weight-bold text-decoration-none">Ruan Scherer</a></span>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>
