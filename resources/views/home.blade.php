@extends('layouts.app')

@section('content')
<div class="w-100 d-flex flex-wrap justify-content-center">
    <div class="col-md-3 w-100 my-2">
        <div class="card rounded-0">
            <div class="card-body">
                <h4 class="font-weight-bold text-dark">Fazer um post</h4>
                <div class="dropdown-divider" ></div>
                <form action="#" method="post" class="mt-3">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control rounded-0" id="content" name="content" placeholder="Diga algo..." required>
                    </div>
                    <div class="form-group">
                        <select class="custom-select rounded-0" required>
                            <option value="bem">Me sinto bem</option>
                            <option value="com suspeita">Estou com suspeita</option>
                            <option value="mal">Me sinto mal</option>
                        </select>
                    </div>
                    <button class="btn btn-outline btn-custom btn-block rounded-0 mt-3">Publicar</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8 w-100 my-2">
        <div class="card">
            <div class="card-header">Dashboard</div>

            <div class="card-body">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                You are logged in!
            </div>
        </div>
    </div>
</div>
@endsection
