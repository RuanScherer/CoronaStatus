<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use App\User;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        if($posts != null)
        {
            return response()->json([
                'posts' => $posts,
                'status' => 'success'
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'fail'
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find($request->user_id);
        if($user != null)
        {
            if(Post::create($request->all()))
            {
                return response()->json([
                    'status' => 'success'
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'fail'
                ]);
            }
        }
        else
        {
            return response()->json([
                'status' => 'fail'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        if($post != null)
        {
            return response()->json([
                'post' => $post,
                'status' => 'success'
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'fail'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showByUser($id)
    {
        $posts = Post::all()->where('user', $id);
        if($posts != null)
        {
            return response()->json([
                'post' => $posts,
                'status' => 'success'
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'fail'
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        if($post != null)
        {
            $post->update($request->all());
            return response()->json([
                'status' => 'success'
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'fail'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if($post != null)
        {
            $post->delete();
            return response()->json([
                'status' => 'success'
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'fail'
            ]);
        }
    }
}
