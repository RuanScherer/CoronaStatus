<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('posts', 'api\PostController');
Route::get('posts/showByUser/{id}', 'api\PostController@showByUser');

/* USERS ROUTES */
Route::get('users', 'api\UserController@index');
Route::get('users/{id}', 'api\UserController@show');
Route::put('users/{id}', 'api\UserController@update');
Route::delete('users/{id}', 'api\UserController@destroy');